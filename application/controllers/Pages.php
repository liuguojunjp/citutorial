<?php

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
       
    }

    public function view($page = 'home') {
        //$this->output->cache(1);
        //$this->output->enable_profiler(TRUE);//激活分析器
        if (!file_exists(APPPATH . '/views/pages/' . $page . '.php')) {
            show_404();
        }
        
        $this->load->helper('url'); //just for helper test
        $this->load->helper('language');
//        $this->load->library('calendar');//-----//caledar test
//        echo $this->calendar->generate(2006, 6);


        $data['title'] = ucfirst($page); // Capitalize the first letter
        $this->load->view('templates/header', $data);
        $this->load->view('pages/' . $page, $data);
        $this->load->view('templates/footer', $data);
    }

}
